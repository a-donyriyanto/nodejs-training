// Cara memanggil/eksekusi function di JS

// Cara 1: Bikin variabel tampung dan eksekusi functionnya
function myFunction(p1, p2){
    return p1 * p2; 
}
result = myFunction(2,3);
console.log(result);

// Cara 2: Tanpa variabel tampung, lasung di eksekusi dan tampilkan
myfunction = (p1, p2) => p1 * p2
console.log(myFunction(2,3));


// Cara 3: Deklarasikan sebuah lambda funtion dan langsung di eksekusi saat itu juga
// Case seperti ini, sangat jarang terjadi dan tidak BEST PRACTICE
// Tetapi ini menekankan pada fleksibilitas dan utk lebih memahami bagaimana JS bekerja
result = function(p1, p2){
    return p1 * p2; 
}(2,3);
console.log(result);



function otherFunction(){
    return "Hi";
}
result1 = otherFunction;  //Apabila tidak menyertakan (), maka tidak akan di eksekusi
result2 = otherFunction();
console.log(result1);       // akan menghasilkan tampilan yang berbeda2
console.log(result1());
console.log(result2);
