// Cara membuat/mendeklarasikan function di JS

// Cara 1
function myFunction(p1, p2){
    return p1 * p2; 
}
console.log(myFunction(2,4));

// Cara 2
myfunction = function(p1, p2){
    return p1 * p2; 
}
console.log(myFunction(2,4));

// Cara 3 (cuman bisa berlaku di ES6)
myfunction = (p1, p2) => p1 * p2
console.log(myFunction(2,4));